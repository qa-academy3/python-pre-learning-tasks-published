def factors(number):
    factors_array = []
    for i in range(2, number-1):
        if number % i == 0:
            factors_array.append(i)
    if not factors_array:
        return (str(number) + " is a prime number")
    else:
        return factors_array

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”

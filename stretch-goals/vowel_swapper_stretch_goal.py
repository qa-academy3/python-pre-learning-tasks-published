


def vowel_swapper(string):
    replace_dict = {"o": "ooo", "O": "000", "a": "4", "A": "4", "e": "3", "E": "3",
                    "i": "!", "I": "!", "u": "|_|", "U": "|_|"}

    new_word = ""
    previous_vowels = ""
    for character in string:
        if character in replace_dict:
            previous_vowels += character.lower()
            #if this is the second of that vowel we've dealt with, replace
            if previous_vowels.count(character.lower()) == 2:
                character = replace_dict[character]
        new_word += character
    return new_word

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console

def factors(number):
    factors_array = []
    for i in range(2, number-1):
        if number % i == 0:
            factors_array.append(i)
    if not factors_array:
        return []
    else:
        return factors_array


print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console

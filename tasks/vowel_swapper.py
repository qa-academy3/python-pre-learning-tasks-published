


def vowel_swapper(string):
    replace_dict = {"o": "ooo", "O": "000", "a": "4", "A": "4", "e": "3", "E": "3",
                    "i": "!", "I": "!", "u": "|_|", "U": "|_|"}
    for character in string:
        if character in replace_dict:
            string = string.replace(character, replace_dict[character])
    return string

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console